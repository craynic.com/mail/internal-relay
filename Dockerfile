FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

ENV POSTFIX_HOSTNAME="" \
    POSTFIX_RELAY_TRANSPORT="" \
    POSTFIX_SMTPD_MILTERS="" \
    POSTFIX_MESSAGE_SIZE_LIMIT="" \
    POSTFIX_REQUIRE_TLS="no"

# renovate: datasource=repology depName=alpine_3_21/postfix depType=dependencies versioning=loose
ARG POSTFIX_VERSION="3.9.3-r0"

COPY files/ /

RUN apk add --no-cache \
        postfix="${POSTFIX_VERSION}" \
        supervisor~=4 \
        inotify-tools~=4 \
        iproute2~=6 \
        bash~=5 \
        run-parts~=4 \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3 \
    && (postfix set-permissions 2>/dev/null || true)

VOLUME /var/spool/postfix

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/probe.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
