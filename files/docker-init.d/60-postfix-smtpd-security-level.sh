#!/usr/bin/env bash

set -Eeuo pipefail

if [[ "$POSTFIX_REQUIRE_TLS" =~ ^(yes|true|1|on)$ ]]; then
  update-postfix-config.sh <(
    echo "smtpd_tls_security_level = encrypt"
  )
fi
