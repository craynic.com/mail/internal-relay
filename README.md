# Internal Mail Relay

Internal mail relay is an e-mail forwarder for internal clients.

## Supported Environment Variables

These environment variables are supported:

* `POSTFIX_RELAY_TRANSPORT` - relay transport as a next-hop transport; mandatory
* `POSTFIX_HOSTNAME` - hostname of this Postfix instance
* `POSTFIX_SMTPD_MILTERS` - space separated list of optional SMTP milter endpoints

## Mountpoints

This container expects the following mandatory mountpoints to be used to supply data:

* `/opt/postfix/ssl/` - mandatory folder with SSL certificates for the SMTPD server; a `tls.crt` or `tls.pem`
  file must be present holding the certificate, optionally including a private key and a CA certificate; the private key
  resp. the certificate can be optionally pased as a `tls.key` resp. a `ca.crt` or `ca.pem` files

Additionally, these optional mountpoints can be used:

* `/opt/postfix/conf.d/` - optional user configuration; files in this folder are merged into Postfix's `main.cf`
* `/opt/postfix/ssl-client/` - mandatory folder with SSL certificates for the SMTP client; a `tls.crt` or `tls.pem`
  file must be present holding the certificate, optionally including a private key and a CA certificate; the private key
  resp. the certificate can be optionally pased as a `tls.key` resp. a `ca.crt` or `ca.pem` files
